#include "triangle.h"

bool isValidTriangle(int a, int b, int c)
{
  return (c > a + b) || (b > a + c) || (a > b + c);
}
